from aiohttp import web
import os

routes = web.RouteTableDef()
uploads_root = './uploads'


def store_file(fname, category, data):
    file_dir = os.path.join(uploads_root, category)
    try:
        os.mkdir(file_dir)
    except Exception as e:
        pass

    with open(os.path.join(file_dir, fname), 'wb') as f:
        f.write(data)


@routes.post('/save')
async def hello(request):
    data = await request.post()
    for category, input_f in data.items():
        bytes = input_f.file.read()
        store_file(input_f.filename, category, bytes)
        print(f'received: [{category}] {input_f.filename} ({len(bytes)} bytes)')
    return web.Response(status=200)


app = web.Application(client_max_size=1024 ** 4)
app.add_routes(routes)
web.run_app(app, port=8787)
