import os
import sys
import wave

import pyaudio
import requests

receiver_url = "http://localhost:8787/save"

chunk = 1024  # Record in chunks of 1024 samples
sample_format = pyaudio.paInt16  # 16 bits per sample
channels = 2
fs = 44100  # Record at 44100 samples per second
seconds = 3
filename = "rec_%s.wav"

store_buff = 100

p = pyaudio.PyAudio()  # Create an interface to PortAudio
SPEAKERS = p.get_default_output_device_info()["hostApi"]  # The part I have modified


def record():
    """
    Записываем в генератор что прочитали.

    :return:
    """
    print('record start')

    output_stream = p.open(format=sample_format,
                           channels=channels,
                           rate=fs,
                           frames_per_buffer=chunk,
                           input=True)

    input_stream = p.open(format=sample_format,
                          channels=channels,
                          rate=fs,
                          frames_per_buffer=chunk,
                          input_device_index=5,  # here is device id
                          input=True,
                          as_loopback=True
                          )

    # Store data in chunks for 3 seconds
    while 1:
        input = yield
        if input:
            print(f'record will stop...')
            break
        data_input = input_stream.read(chunk)
        data_output = output_stream.read(chunk)
        yield (data_input, data_output)

    # Stop and close the stream 
    input_stream.stop_stream()
    input_stream.close()
    output_stream.stop_stream()
    output_stream.close()

    # Terminate the PortAudio interface
    p.terminate()
    print('record finished')


def store(frames, id):
    fname = filename % id
    wf = wave.open(fname, 'wb')
    wf.setnchannels(channels)
    wf.setsampwidth(p.get_sample_size(sample_format))
    wf.setframerate(fs)
    wf.writeframes(b''.join(frames))
    wf.close()
    print(f'record stored to: {fname}')
    return fname


def clean(start, stop):
    for i in range(start, stop):
        try:
            os.remove(filename % i)
            print(f'file {filename % i} removed')
        except Exception as e:
            print(f'error at remove {filename % i} {e}')


def sent(fname, data_identity):
    with open(fname, 'rb') as f:
        response = requests.post(receiver_url, files={data_identity: f})

    print(f'record sent ok? {response.status_code == 200}')


def run(seconds, identity):
    buff_in = []
    buff_out = []
    counter = 0
    need_sent_count = (fs * seconds) // chunk

    record_gen = record()
    try:
        for x in record_gen:
            if x:
                frames_in, frames_out = x
                buff_in.append(frames_in)
                buff_out.append(frames_out)

            if len(buff_in) >= need_sent_count:
                fname_in = store(buff_in, f'{counter}_in')
                fname_out = store(buff_out, f'{counter}_out')
                buff_in = []
                buff_out = []
                sent(fname_in, f'{identity}_in')
                sent(fname_out, f'{identity}_out')
                counter += 1
                if counter >= store_buff:
                    start, stop = store_buff - counter, store_buff - counter + 1
                    print(f'will clean: {start} {stop}')
                    clean(start, stop)

    except Exception as e:
        record_gen.send('stop')


def main(args: list):
    if len(args) != 1:
        print(f'if you run with name provided: python record.py <some name where you record> it will be identity')
        identity = '-'
    else:
        identity = args[0]
    print(f'your identity: {identity}')

    run(seconds, identity)


if __name__ == '__main__':
    main(sys.argv[1:])
